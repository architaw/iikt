string = input('Enter string: ')

alphabet = string.lower()
dictionary = {}

for letters in alphabet:
    dictionary[letters] = 0

for letters in alphabet:
    dictionary[letters] += 1

dictionary = sorted(dictionary.items(),
                    reverse=True,
                    key=lambda x: x[1])

for position in range(0, len(dictionary)):
    print(str(list(dictionary[position])[0]) +
          ' - ' + str(list(dictionary[position])[1]))

    if position != len(dictionary) - 1:
        if dictionary[position + 1][1] < dictionary[position][1]:
            break
