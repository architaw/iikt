# -*- coding: utf-8 -*-

def print_arr(arr, n, m):
    for i in range(n):
        for j in range(m):
            print(arr[i][j], end=' ')
        print()


n = int(input('Введите количество строк '))
m = int(input('Введите количество столбцов '))

arr = []

for i in range(n):
    sub_arr = []

    for j in range(m):
        el = int(input('Введите [ ' + str(i) + ' , ' + str(j) + ' ] элемент '))
        sub_arr.append(el)

    arr.append(sub_arr)

print('Исходная матрица: ')
print_arr(arr, n, m)

upper_and_main_d = []
lower_main_d = []

for i in range(n):
    for j in range(m):
        el = arr[i][j]
        if(j < i):
            lower_main_d.append(el)
        else:
            upper_and_main_d.append(el)

print(upper_and_main_d)
print(lower_main_d)


def find_sum(arr1, arr2):
    sum = 0
    for val in arr1:
        if(max(arr2 + [val]) == val):
            sum = sum + val

    if(sum == 0):
        print('Hasnt valid els')
    else:
        print(sum)


find_sum(upper_and_main_d, lower_main_d)
