# -*- coding: utf-8 -*-

input_str = input('Enter array: ')

arr = list(map(int, input_str.split(' ')))

max_arr_int = min(arr)
max_arr_ind_idx = arr.index(max_arr_int)

print('Min arr el: ', max_arr_int)
print('Min arr el idx: ', max_arr_ind_idx)
