# -*- coding: utf-8 -*-

input_str = input('Enter array: ')

arr = list(map(int, input_str.split(' ')))

pos_arr = []
neg_arr = []

for val in arr:
    if val >= 0:
        pos_arr.append(val)
    else:
        neg_arr.append(val)

print('Positive arr: ', pos_arr)
print('Negative arr: ', neg_arr)
