import math

x = int(input('X: '))

res = math.sin(math.sqrt(x + 1)) - math.sin(math.sqrt(x - 1))

print(res)
