import math

x = int(input('X: '))

res = (0.125*x + math.fabs(math.sin(x))) / (1.5*(x*x) + math.cos(x))

print(res)
