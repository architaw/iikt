# -*- coding: utf-8 -*-

def fac(n):
    if n == 0:
        return 1
    return fac(n-1) * n


n = int(input('Введите n: '))
m = int(input('Введите m: '))

c = fac(m)/(fac(n)*fac(m - n))

print('Результат c: ', c)
