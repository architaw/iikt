# -*- coding: utf-8 -*-

from collections import Counter

str = input('Enter str: ')
substr = input('Enter substr: ')

odd_chars = ''
even_chars = ''

upper_chars_count = 0
lower_chars_count = 0

unique_str = "".join(dict.fromkeys(str))

str_counter = Counter(str)

reversed_str = str[::-1]

for i, val in enumerate(str):
    idx = i + 1

    if idx % 2 == 0:
        even_chars = even_chars + val
    else:
        odd_chars = odd_chars + val

    if val.isupper():
        upper_chars_count = upper_chars_count + 1
    else:
        lower_chars_count = lower_chars_count + 1


print('Odd: ', odd_chars)
print('Even: ', even_chars)

print('Upper count: ', upper_chars_count)
print('Lower count: ', lower_chars_count)

upper_lower_chart_ratio = (upper_chars_count / lower_chars_count) * 100

print('Upper/Lower count: ', upper_lower_chart_ratio, '%')

print('Unique str: ', unique_str)

print('Str counter: ',  str_counter)

print('Reversed str: ', reversed_str)

print('Str w/ substr: ', str.join(substr))

print('Str w/o substr', str.replace(substr, ''))

print('Str length', len(str))
