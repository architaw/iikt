# -*- coding: utf-8 -*-

def print_arr(arr, n, m):
    for i in range(m):
        for j in range(n):
            print(arr[i][j], end=' ')
        print()


n = int(input('Введите количество строк и столбцов '))

arr = []

for i in range(n):
    sub_arr = []

    for j in range(n):
        el = int(input('Введите [ ' + str(i) + ' , ' + str(j) + ' ] элемент '))
        sub_arr.append(el)

    arr.append(sub_arr)

print_arr(arr, n, n)

arr_row_sum = sum(arr[0])

if all([sum(x) == arr_row_sum for x in arr]) and all([sum(x) == arr_row_sum for x in list(zip(*arr))]):
    print('Матрица является магическим квадратом')
else:
    print('Матрица НЕ является магическим квадратом')
