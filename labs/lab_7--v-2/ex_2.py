# -*- coding: utf-8 -*-

def print_arr(arr, n, m):
    for i in range(n):
        for j in range(m):
            print(arr[i][j], end=' ')
        print()


n = int(input('Введите количество строк '))
m = int(input('Введите количество столбцов '))

arr = []

for i in range(n):
    sub_arr = []

    for j in range(m):
        el = int(input('Введите [ ' + str(i) + ' , ' + str(j) + ' ] элемент '))
        sub_arr.append(el)

    arr.append(sub_arr)

print('Исходная матрица: ')
print_arr(arr, n, m)

for i in range(n):
    col = arr[i]

    firstColEl = col[0]
    lastColEl = col[-1]

    col[0] = lastColEl
    col[-1] = firstColEl

print('Измененная матрица: ')
print_arr(arr, n, m)
