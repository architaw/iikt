a = int(input())

res = 0
k = 1

while res < a:
    k = k + 1
    res = 2 ** k

print(res)
