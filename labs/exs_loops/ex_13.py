input_str = input()

arr = list(map(int, input_str.split(' ')))

res = 0

valid_arr = []

for val in arr:
    if val % 3 == 0:
        valid_arr.append(val)

arr_sum = sum(valid_arr)

print(arr_sum / len(valid_arr))
