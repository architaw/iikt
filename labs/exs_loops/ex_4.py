import math

n = input()

n_arr = list(map(int, n))

res = 0

for k in n_arr:
    res = res + k ** 2

print(res)
