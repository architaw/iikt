import math

n = int(input())

res = 0

for k in range(1, n):
    res = res + ((k ** 3) / math.tan(k + math.sin(k)))

print(res)
