import math

n = int(input())

res = 0

x = 2
y = 4

z = 0

for i in range(n):
    res += (math.sin(x)/y)
    x += 2
    y = (y + 2) ** 2

print(res)
