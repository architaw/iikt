import math

n = int(input())

res = 0

for k in range(1, n):
    res = res + (((math.cos(k) / math.sin(k)) ** 2) / (2 * k - 1))

print(res)
