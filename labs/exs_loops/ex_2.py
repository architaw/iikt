import cmath

n = int(input())

res = 0

for k in range(1, n):
    res = res + (cmath.exp(1 / k) / cmath.sqrt(k + cmath.tan(k)))

print(res)
