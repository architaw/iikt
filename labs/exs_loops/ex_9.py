import math

n = int(input())

res = 0

x = 1
y = 3

for i in range(n):
    res = res + (math.sin(x + i) / (y + i))

print(res)
