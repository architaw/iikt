input_str = input()

arr = list(map(int, input_str.split(' ')))

count = 0

for val in arr:
    if -2 <= val <= 18:
        count = count + 1

print(count)
