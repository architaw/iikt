import math

n = int(input())

res = 0

x = 2
y = 5

for i in range(n):
    res = res + (math.sin(x + i) / (y + i))

print(res)
